//const dotenv=require('dotenv');
var express = require('express');
var app = express();
var path = require('path');
// dotenv.config({
// 	path: path.resolve(__dirname,'./.env')
// })
const sgMail = require('@sendgrid/mail');
// 
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
module.exports.mailsendOtp=function(x,y){
const msg ={
    to: x,
    from:"noreply@gangwarexpressapi.com",
    subject: 'OTP for Authorizing yourself', 
    html:`Hi, <br> Please verify yourself by using this OTP: ${y}`  
}
sgMail.send(msg,(err,res)=>{
    if(err){
        console.log("sent failed",err)
    } else {
        console.log("sent")
    }
})
}

module.exports.mailsend=function(x,y){
    const msg ={
        to: x,
        from:"noreply@gangwarexpressapi.com",
        subject: 'Confirm your Email address.', 
        html: `<div style="font-family:inherit; text-align: inherit"><strong>Hi,</strong></div>
        <div style="font-family: inherit; text-align: inherit"><br></div>
        <div style="font-family: inherit; text-align: inherit"><strong>This email regarding the registration of your account on anirudhexpress-api.</strong></div>
         <div style="font-family: inherit; text-align: inherit"><strong>Please click on below button to confirm your email address.</strong></div>
        
        <a href='https://gangwarexpress-oauth-api.herokuapp.com/verify/${y}' style="background-color:#19aff0; border:1px solid #6cd1dd; border-color:#6cd1dd; border-radius:6px; border-width:1px; color:#ffffff; display: inline-block; font-size:14px; font-weight: normal; letter-spacing:1px; line-height: normal; padding:12px 18px 12px 18px; text-align: center; text-decoration: none; border-style: solid; font-family: courier, monospace;" target="_blank">Confirm Email</a>
        
        <div style="font-family: inherit; text-align: inherit"><strong>Thanks and Regards,</strong></div>
        
        <div style="font-family: inherit; text-align: inherit"><strong>Anirudh.</strong></div>`
    }
    sgMail.send(msg,(err,res)=>{
        if(err){
            console.log("sent failed",err)
        } else {
            console.log("sent")
        }
    })
    }

