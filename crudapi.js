//const dotenv=require('dotenv');
var express = require('express');
var app =express();
var mailsender=require('./mailsender.js');
var tokenjwt=require('./tokenjwt.js');
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var mongoose = require('mongoose');
const path = require('path');


// console.log(path.resolve(process.cwd(), '.env'));
// const result = dotenv.config({
// 	path: path.resolve(__dirname,'./.env')
// })


mongoose.connect(process.env.URL,{ useUnifiedTopology: true, useNewUrlParser: true});
var otplib=require('otplib');
var x;
var schemadb = mongoose.Schema({

	name:String,
	email:String,
	password:String,
    status: {
    	type:String,
    	default:'unverified'
    },
    key:String,
    MobileNo:Number
	

});

var schemamodel = mongoose.model("register", schemadb);
const port =process.env.PORT || 3000;
debugger
app.listen(port, function(err,data){
	if(err){
		console.log("error in starting");
	} else{
 console.log('listening on port : ',port);
	}
});
app.get('/', function(req, res) {
  schemamodel.find(function(err,resp){
   res.json(resp);
     });


});
app.post('/signup',function(req,res){
	var datadb = new schemamodel(req.body);
	if(!req.body.email || !req.body.password){
		res.status("400");
	    res.send("invalid details");
} else {
	schemamodel.findOne({email:req.body.email},function(err,schemamodel){
   if (err) return res.status(500).send('error on server');
   if (schemamodel){ 
   	return res.status(200).send('user already existing'); } else {
datadb.save().then(item => {
	res.send("Sir, You are registered. A verification link is sent on your mail please check your mail");
  var y=tokenjwt.assign(req.body.email);
  console.log(y);
	mailsender.mailsend(req.body.email,y);
}).catch(err => {
res.send("error in saving the data");
});
}
     });
   }
});
app.post('/login',function(req,res){
  schemamodel.findOne({email:req.body.email},function(err,schemamodel){
  	if (err) return res.status(500).send('error on server');
  	if (!schemamodel) { return res.status(404).send("you are not registered");} else {
  		
  	var pwd1=req.body.password;
  	var pwd2=schemamodel.password;
  	 // var nme=schemamodel.name;
  	if (schemamodel && pwd1===pwd2 && schemamodel.status==='verified') {
  		login(schemamodel.MobileNo); //ye function is defined below.

  } else {
 
  	if (pwd1===pwd2 && schemamodel.status==='unverified') {
  			 res.send("Not verified please verify your mail a mail is sent to you");
  			 var x=tokenjwt.assign(req.body.email);
             console.log(x);
	         mailsender.mailsend(req.body.email,x,(error,data)=>{
				 if(error){
					 console.log("unable to send email")
				 } else{
					 console.log("mail sent successfully")
				 }
			 });
  		} 
  		
  }
  if (schemamodel && pwd1!==pwd2){ return res.send("Password is incorrect");}  	
}

  }); 
  function login(x){ 
  	otplib.authenticator.options = {
  step: 60,
  window: 1
};
  	const secret = otplib.authenticator.generateSecret();
        const token = otplib.authenticator.generate(secret);
  	schemamodel.findOneAndUpdate({email:req.body.email},{$set:{key:secret}},function(err,schemamodel){
          if (err) { 
          	res.send("some internal error");
      }
           console.log(token);
           console.log(x);
           mailsender.mailsendOtp(req.body.email,token);
           res.send("A token/OTP is sent to you on your Mail. Please check your mail");
  		});
  }
});

app.get('/verify/:id',function(req,res){
	var y=tokenjwt.verifytoken(req.params.id);
	 console.log(y.obj);
	schemamodel.findOneAndUpdate({email:y.obj},{ $set: { "status" : 'verified' } },function(err,schemamodel){
		if(err) return res.status(500).send("error in fetching");
		if(!schemamodel) {return res.status(500).send("invalid token"); } else {
			res.send("Verified now Please sign in now");
			 
		}
	});
});
app.post('/verify/otp',function(req,res){
schemamodel.findOne({email:req.body.email},function(err,schemamodel){
if(err){res.send('some error in verification of otp');}
else {
	var z=req.body.otp;
	var m =schemamodel.key;
const isValid = otplib.authenticator.check(z,m);
//console.log(z+isValid+" "+schemamodel.key);
if (isValid) {
	return res.status(200).send(schemamodel.name+' Sir You are logged in.');
} else {
	res.send("invalid otp");
}
}
});
});